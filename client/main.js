import { Meteor } from 'meteor/meteor';
import { Session } from 'meteor/session';
import { Template } from 'meteor/templating';
import './menu.html';

Meteor.startup(()=>{
});

Menus = {};  // Global dictionary containing menu collections for each dispensary

// subscribe to collections 
Meteor.subscribe("usersData");
Meteor.subscribe("dispensaries");

// set some default session variables
Session.set('searchQuery','');
Session.set('sortQuery', {"price.gram.data": -1});
Session.set('sortQueryName','Highest Gram Price');
if ($(window).width() < 992){
    Session.set('showNitemsInit', 1 + Math.round($(window).height()/177));
}else{
     Session.set('showNitemsInit', 1 + Math.round($(window).height()/72));
}
Session.set('showNitems', Session.get('showNitemsInit'));

// Load items as user scrolls
$(window).on('scroll', function() {
    if($(window).scrollTop() > 20) {
	$('.back-to-top').removeClass('hide');
    } else {
	$('.back-to-top').addClass('hide');
    }
    var showNitems =  Session.get('showNitemsInit')+parseInt($(window).scrollTop()/100);
    if (showNitems > Session.get('showNitems')){
	Session.set('showNitems', showNitems);
    }
 });

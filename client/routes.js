import { Meteor } from 'meteor/meteor';
import { Session } from 'meteor/session';

// Routes

Router.route('/', {
    layoutTemplate:"AdminLayout",
    yieldRegions:{
        'navbar': {to: "navbar"},
	'dispensaryForm': {to: "dispensaryForm"},
	'dispensaryList': {to: "dispensaryList"},
	'about': {to: "about"}
    },
    action: function(){
        this.render();
    },
    onAfterAction: function () {
    },
    onStop: function () {
        
    }
});
	    

Router.route('/:dispensary_name',{
    yieldRegions:{
        'MenuNav': {to:"MenuNav"},
        'FullMenu': { to:"Menu"},
	'ItemModal': {to:"ItemModal"}
    },
    waitOn: function () {
	return Meteor.subscribe("dispensaries");
    },
    onBeforeAction: function(){
	if(! this.data()){
	    this.redirect('/');
	} else {
	    if (!Menus[this.params.dispensary_name]){
		Menus[this.params.dispensary_name] = new Mongo.Collection( this.params.dispensary_name);
	    }
	    Meteor.subscribe(this.params.dispensary_name);
	    Session.set('dispensaryName',  this.params.dispensary_name);
	    Session.set('SelectedCategory', 'All Categories');
	    this.next();
	}
    },
    action: function(){
        this.render();
    },
    onAfterAction: function () {
	$('.js-AllCategories-link').addClass('selected');
    },
    onStop: function () {
        $('.js-AllCategories-link').removeClass('selected');
    },
    data: function(){
	var dispensary = Dispensaries.findOne({'name': this.params.dispensary_name});
	return dispensary;
    }
});

Router.route('/:dispensary_name/:category',{
    yieldRegions:{
        'MenuNav': {to:"MenuNav"},
        'Category': { to:"Menu"},
	'ItemModal': {to:"ItemModal"}
    },
    onBeforeAction: function(){
	    if (!Menus[this.params.dispensary_name]){
		Menus[this.params.dispensary_name] = new Mongo.Collection( this.params.dispensary_name);
	    }
	    Meteor.subscribe(this.params.dispensary_name);
	    Session.set('dispensaryName',  this.params.dispensary_name);
	    Session.set('SelectedCategory', this.params.category);
	    this.next();
    },
    action: function(){
        this.render();
    },
    onAfterAction: function () {
	$('.js-'+this.params.category+'-link').addClass('selected');
    },
    onStop: function () {
        $('.js-'+this.params.category+'-link').removeClass('selected');
    },
    data: function(){
	var Menu = Menus[this.params.dispensary_name];
	var category =	{'category':{'category': this.params.category,
				     'nItems': Menu.find({category: this.params.category}).count()}};
	return category;
    }
});



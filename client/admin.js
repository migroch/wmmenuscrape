// Admin page helpers and events

Template.dispensaryForm.events({
    "click .js-toggle-dispensary-form":function(event){
        $("#dispensary_form").toggle('slow');
    }, 
    "submit .js-save-dispensary-form":function(event){
        //  save dispensary
        var name = event.target.name.value;
        if (Meteor.user()){
            Meteor.call('AddDispensary', name);
        }
        else {
            window.alert("Please sign in to add a dispensary");
        }
        return false;// stop the form submit from reloading the page
    }
});

Template.about.events({
    "click .js-toggle-dispensary-form":function(event){
        $("#dispensary_form").toggle('slow');
    }
});
    
Template.dispensaryList.helpers({
    getDispensaries:function(){
	return Dispensaries.find();
    }
});

// Menu related helpers and events


//Events
Template.MenuLayout.events({
    'click': function(){
	if($('#catList').hasClass('in')){
	    $('#catList').collapse('hide');
	}
	if($('.js-sort-tool').hasClass('in')){
	    $('.js-sort-tool').collapse('hide');
	}	
    },
    'click .back-to-top':function(){
	$(window).scrollTop(0);
    }
});

Template.MenuNav.events({
    'click .js-category-tool' : function(){
	if ($('#catList').hasClass('in')){
	    $('#catList').collapse('hide');
	}	
    },
    'keyup .js-search-input': _.throttle(function(event){
        var text = $(event.target).val().trim();
        //MenuItemSearch.search(text);
	Session.set('searchQuery', text);
    }, 1000),
    'click .js-search-close': function(){
	Session.set('searchQuery','');
    }
});

Template.MenuItem.events({
    'click .wm-menu-item':function(event){
	var card = $(event.target).closest('.js-item-card')[0];
	$(".item-modal-body").html($(card).html());
	$('#itemModal').modal();
    }
});

Template.ItemModal.events({
    'click .modal-dialog':function(event){
	if ($(event.target)[0] == $('.wm-menu-item.dialog')[0]){
	    $('#itemModal').modal('hide');
	}
    }
});

Template.SortTool.events({
    'click .js-sort-highTHC': function () {
	Session.set('sortQuery', {"THC.testData": -1});
	Session.set('sortQueryName','Highest THC');
    },
    'click .js-sort-highCBD': function () {
	Session.set('sortQuery', {"CBD.testData": -1});
	Session.set('sortQueryName','Highest CBD');
    },
    'click .js-sort-lowTHC': function () {
	Session.set('sortQuery', {"THC.testData": 1});
	Session.set('sortQueryName','Lowest THC');
    },
    'click .js-sort-lowCBD': function () {
	Session.set('sortQuery', {"CBD.testData": 1});
	Session.set('sortQueryName','Lowest CBD');
    },
    'click .js-sort-highHALFGRAM': function () {
	Session.set('sortQuery', {"price.half_gram.data": -1});
	Session.set('sortQueryName','Highest 0.5 Gram Price');
    },
    'click .js-sort-highGRAM': function () {
	Session.set('sortQuery', {"price.gram.data": -1});
	Session.set('sortQueryName','Highest Gram Price');
    },
     'click .js-sort-highEIGHTH': function () {
	Session.set('sortQuery', {"price.eighth.data": -1});
	Session.set('sortQueryName','Highest Eighth Price');
     },
     'click .js-sort-highOZ': function () {
	Session.set('sortQuery', {"price.ounce.data": -1});
	Session.set('sortQueryName','Highest Ounce Price');
     },
    'click .js-sort-highUNIT': function () {
	Session.set('sortQuery', {"price.unit.data": -1});
	Session.set('sortQueryName','Highest Unit Price');
    },
    'click .js-sort-lowHALFGRAM': function () {
	Session.set('sortQuery', {"price.half_gram.data": 1});
	Session.set('sortQueryName','Lowest 0.5 Gram Price');
    },
    'click .js-sort-lowGRAM': function () {
	Session.set('sortQuery', {"price.gram.data": 1});
	Session.set('sortQueryName','Lowest Gram Price');
    },
     'click .js-sort-lowEIGHTH': function () {
	Session.set('sortQuery', {"price.eighth.data": 1});
	Session.set('sortQueryName','Lowest Eighth Price');
     },
     'click .js-sort-lowOZ': function () {
	Session.set('sortQuery', {"price.ounce.data": 1});
	Session.set('sortQueryName','Lowest Ounce Price');
     },
    'click .js-sort-lowUNIT': function () {
	Session.set('sortQuery', {"price.unit.data": 1});
	Session.set('sortQueryName','Lowest Unit Price');
    }
});


// Helpers
Template.SortTool.helpers({
    getSortQueryName:function(){
	return Session.get('sortQueryName');
    }
});

Template.MenuNav.helpers({
    getCurrentDispensary:function(){
	return Session.get('dispensaryName');
    },
    getCategories: function(){
	var categories = Session.get('categories');
	if (categories){
	    return categories;
	}else{
	    if (Session.get('dispensaryName')){
		categories = Meteor.call('GetCategories', Session.get('dispensaryName'),
					 function(err, result){
					     Session.set('categories', result);
					     return result;
					 });
	    }
//	    categories = Meteor.call('GetCategories', Session.get('dispensaryName'));
//	    Session.set('categories', categories);
//	    return categories;
	}
    },
    getSelectedCategory: function(){
	var selectedCategory = Session.get('SelectedCategory');
	return selectedCategory;
    }
});

Template.FullMenu.helpers({
    getCategories: function(){
	var categories = Session.get('categories');
	if (categories){
	    return categories;
	}else{
	    if (Session.get('dispensaryName')){
		categories = Meteor.call('GetCategories', Session.get('dispensaryName'),
					 function(err, result){
					     Session.set('categories', result);
					     return result;
					 });
	    }
	}
    }
});

Template.MenuItem.helpers({
    getItemPrice: function(itemId){
	var Menu = Menus[Session.get('dispensaryName')];
	var item = Menu.find({'_id': itemId}).fetch()[0];
	return _.values(item.price);
    }
});

Template.Category.helpers({
    getMenuItems: function(catName){
	var searchQuery, searchText = Session.get('searchQuery');
	var sortQuery = Session.get('sortQuery');
	var limit = Session.get('showNitems');
	var options = {'limit': limit, 'sort': sortQuery};
	var Menu = Menus[Session.get('dispensaryName')];

	if(searchText) {
	    var regExp = buildRegExp(searchText);
	    var selector = {$and:[{category: catName},
	 			  { $or: [{name: regExp}, {description: regExp}]}]};
	    return Menu.find(selector, options).fetch();
	} else {
	    return Menu.find({category: catName}, options).fetch();
	}
    }
});


// Miscellanious functions
function buildRegExp(searchText) {
  var words = searchText.trim().split(/[ \-\:]+/);
  var exps = _.map(words, function(word) {
    return "(?=.*" + word + ")";
  });
  var fullExp = exps.join('') + ".+";
  return new RegExp(fullExp, "i");
}

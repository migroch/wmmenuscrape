var system = require('system');
var webPage = require('webpage');
var fs = require('fs');
// Scrape the menu from the Weedmaps site for the given dispensary name 
// and write it to a JSON file

// Set url based on the given dispensary name
var dispensary_name = system.args[1];
var timeOut =system.args[2] ;
var path = system.args[3];
var url = 'https://weedmaps.com/dispensaries/'+dispensary_name+'#/menu';
console.log('Loading '+url);

// Create WM menu page
var page = webPage.create();
page.settings.loadImages = 'false';

page.onConsoleMessage = function(msg) {
    // print to stdout console messages 
    system.stdout.write('page console: ' + msg);
};

// Load page and get menu
page.open(url, function(status) {
    if (status !== 'success') {
	console.log('ERROR: Unable to access network');
	phantom.exit();    
    } else {
	setTimeout(function(){
	    try{
		getMenu(page);
	    }
	    catch(err){
		console.log('ERROR: '+err);
		phantom.exit();
	    }
	}, timeOut);
    }
});

page.onCallback = function(menu) {
    // Save menu data on file.
    // menu is the JSON data object sent by page
    try{
	var fileName = dispensary_name+'_menu.json';
	console.log('Saving data to '+ path+'/'+fileName);
	writeToFile(menu, fileName);
    }
    catch(err){
	console.log('ERROR: '+err);
	phantom.exit();  
    }
};

function writeToFile(data, fileName){
    //Write data to file
    fs.write(path+'/'+fileName, JSON.stringify(data) , 'w');
    console.log('Data written to '+fileName+' succesfully!');
    phantom.exit();
}

page.onError = function(msg, trace){
    // Errors from page
    var msgStack = ['ERROR: ' + msg];
    if (trace && trace.length) {
	msgStack.push('TRACE:');
	trace.forEach(function(t) {
	    msgStack.push(' -> ' + t.file + ': ' + t.line + (t.function ? ' (in function "' + t.function +'")' : ''));
	});
    }
    console.error(msgStack.join('\n'));
    //phantom.exit();
};

function  getMenu(page){ 
    // get Menu from page
    var jquery = "https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js";
    var menu = page.includeJs(jquery, function(){
	page.evaluate(function(){
	    // Evaluate page in sandbox to get the menu data

	    var Nitems = $('.wm-menu-item-card').length;
	    console.log('Found '+Nitems+' items in the menu');
	    
	    // Fill the menu array
	    var menuArray = [];
	    $('.wm-menu-item-card').each(function(index){
		// Iterate over menu items
		//console.log(index);
		var menuItem = {
		    'index': index,
		    'category': $(this).find('.wm-menu-item-category').text(),
		    'name': $(this).find('.wm-menu-item-name').text(),
		    'photo_url': $(this).find('.wm-menu-item-photo').attr('style')
			.replace('background-image: url(','')
			.replace(')',''),
		    'description': $(this).find('.wm-menu-item-body').text(),
		    'THC': {
			'testData': $(this).find('.thc').find('.wm-menu-item-test-data').
			    text().trim().replace('%',''),
			'testLabel': $(this).find('.thc').find('.wm-menu-item-test-label').text(),
			'testDataLabel': $(this).find('.thc').find('.wm-menu-item-test-data-label').text()
		    },
		    'CBD':{
			'testData': $(this).find('.cbd').find('.wm-menu-item-test-data').
			    text().trim().replace('%',''),
			'testLabel': $(this).find('.cbd').find('.wm-menu-item-test-label').text(),
			'testDataLabel': $(this).find('.cbd').find('.wm-menu-item-test-data-label').text()
		    },
		    'CBN': {
			'testData': $(this).find('.cbn').find('.wm-menu-item-test-data').
			    text().trim().replace('%',''),
			'testLabel': $(this).find('.cbn').find('.wm-menu-item-test-label').text(),
			'testDataLabel': $(this).find('.cbn').find('.wm-menu-item-test-data-label').text()
		    },
		    'testDate': $(this).find('.wm-menu-item-test-date').text().trim()
		};
		var itemPriceDict = {};
		$(this).find('.wm-menu-item-price').each(function(){
		    // Iterate over price labels
		    var priceLabel = $(this).find('.wm-menu-item-price-label').text();
		    var priceData = $(this).find('.wm-menu-item-price-data').text().trim().split('.')[0];
		    var minorUnit = $(this).find('.wm-item-price-minor-unit').text();
		    var key = $(this).attr("class").split(" ").slice(-1)[0];
		    itemPriceDict[key] = {'key':key, 'label': priceLabel, 'data': priceData,
					  'minorUnit': minorUnit};
		});
		menuItem.price = itemPriceDict; // Add prices to the menuItem object
		menuArray.push(menuItem); // Add this menuItem to the menu array
	    });

	    // Done populating the buffer.
	    if (menuArray.length == Nitems){
		// CallBack and pass menu data to phantom server
		console.log('Menu data scraped succesfully!');
		var menu = {'menu':menuArray};
		window.callPhantom(menu);
	    } else {
		console.log('err');
		var err = "Menu array doesn't have the expected number of itmes";
		throw(err);
	    }
	    //end of page.evaluate()
	});
	//end of includeJS()
    });
    //end of getMenu()
}

/////////////  To try in the future, NOT WORKING  ////////////

	// // wait to finish loading
	// wait = 1; wait_mssg = 1;
	// Nitems = 0;
	// NitemsNow = $('.wm-menu-item-card').length;
	// var  loadCheck = function(){
	// 	console.log('loadCheck');
	// 	Nitems = NitemsNow;
	// 	NitemsNow = $('.wm-menu-item-card').length;
	// 	wait = 1;
	// 	wait_mssg = 1;
	// };
	
	// while (NitemsNow > Nitems){
	// 	if (wait){
	// 	    wait = 0;
	// 	    window.setTimeout(loadCheck(), 10000);
	// 	} else {
	// 	    if (wait_mssg) console.log('Waiting for all items to load, N items last = ', NitemsNow);
	// 	    wait_mssg = 0;
	// 	}
	// }

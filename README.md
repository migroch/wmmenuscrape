# README #

This is a custom tool build by [SciTech Analytics](https://scitecha.com) for [Kushmoda](https://kushmoda.com). With this tool [Kushmoda](https://kushmoda.com) can scrape the menu data of dispensaries from Weedmaps.

### How to use ###

1. Click on the **ADD DISPENSARY** button to initialize scraping for a new dispensary.

2. On the form type the Dispensary-Name of the dispensary you would like to add to the list of dispensaries for which menus are being scraped. The Dispensary-Name should be as it appears in the Weedmaps url for the given dispensary. For example, the Herbal Cruz dispensary menu can be found in https://weedmaps.com/dispensaries/herbal-cruz, thus the Dispensary-Name you would have to type on the **ADD DISPENSARY** form would be hearbal-cruz.

3. Click submit. After 2-5 minutes the menu for the newly added dispensary should be visible at https://kushmenus.scitecha.com/Dispensary-Name

### What is happening on the back ###

Once you add a dispensary to the list, the menu for that dispensary on Weedmaps will be scraped every 15 minutes. This is done via a PhantomJS child process being launched for each dispensary on the list every 15 minutes. The PhantomJS script writes the menu data of each dispensary to a JSON file, and each time these JSON files are modified the application updates the data into a MongoDB database. 

### Deployment ###

This is a Meteor application that can be run on any server via Node.js. The source code for this application can be found at https://bitbucket.org/migroch/wmmenuscrape.

To deploy on your own server follow the instructions here (use Meteor 1.3.x) https://guide.meteor.com/deployment.html#custom-deployment

### Contributors List ###

* Miguel Rocha - miguel@scitecha.com
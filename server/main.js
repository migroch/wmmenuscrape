import { Meteor } from 'meteor/meteor';
import phantomjs from 'phantomjs-prebuilt';
import {spawn} from 'child_process';
import fs from 'fs';

Meteor.startup(() => {
    var dispensaries = Dispensaries.find();
    dispensaries.forEach( function(dispensary){
	Meteor.call('AddDispensary', dispensary.name);
    });
});

Menus = {};  // Global dictionary containing menu collections for each dispensary

// publish collections for read access 
Meteor.publish("usersData", function(){
    return Meteor.users.find();
});

Meteor.publish("dispensaries", function(){
    return Dispensaries.find();
});

Meteor.methods({
    // Server methods

    'AddDispensary'(dispensaryName){
	// Set up the menu for a new dispensary
	if (Dispensaries.findOne({name: dispensaryName})){
	    console.log('Initializing already added dispensary: '+dispensaryName);
	}
	else{
	    console.log('Adding new dispensary: '+dispensaryName);
	    Dispensaries.insert({
		name: dispensaryName,
		addedOn: new Date(),
		addedBy: Meteor.user().username
            });
	}
	try{
	    var Menu = new Mongo.Collection( dispensaryName) ;
	    Menus[dispensaryName] = Menu;
	    Meteor.publish(dispensaryName, function(){
		return Menu.find();
	    });
	} catch(err) {
	    console.log(err, dispensaryName+' added more than once?');
	}
	Meteor.call('StartScrapeAndUpdate', dispensaryName);
    },
    
    'StartScrapeAndUpdate'(dispensaryName){
	// Start w the wm menu for the given dispensary
	console.log('Initializing Scrape and Update for '+dispensaryName );
	var menuFile = dispensaryName+'_menu.json';
	var path = 'private/';

	var TimeOut = 60000; // time to wait for loading
	Meteor.call('ScrapeMenu', dispensaryName, TimeOut, path);  
	Meteor.setInterval(function() {
	    Meteor.call('ScrapeMenu', dispensaryName, TimeOut, path);  
	}, 60000*15); 	    // Srape every 15 minutes

	var watchFile = Meteor.wrapAsync(fs.watchFile);
	watchFile(path+menuFile, (curr, prev) => {
	    // watch menuFile and update data base if modified
	    console.log(menuFile + ' modified. Updating data in mongoDB');
	    var menuData = JSON.parse(fs.readFileSync(path+menuFile));
	    var menuArr = menuData.menu;
	    var Menu =  Menus[dispensaryName];
	    var testInsert = Menu.insert(menuArr[0]);
	    if(testInsert){
		Menu.remove({});
		for(var i = 0; i < menuArr.length; i++) {
		    var thisItem = menuArr[i];
		    Menu.insert(thisItem);
		}
	    }
	});
	//end of StartScrapeAndUpdate
    },
    
    'ScrapeMenu'(dispensaryName, timeOut, path){
	// Spawn a phantomjs child process tha scrapes
	// and saves the wm menu in a JSON file
	console.log('Scraping Menu for '+ dispensaryName);
	command = spawn(phantomjs.path,
			['assets/app/menuScraper_phantom.js',
			 dispensaryName, timeOut, path]);
	command.stdout.on('data',  function (data) {
	    console.log('phantomjs stdout: ' + data);
	});
	command.stderr.on('data', function (data) {
	    console.log('phantomjs stderr: ' + data);
	});
	command.on('exit', function (errCode) {
	    if (errCode) {
		console.log('ERROR: child process exited with code ' + errCode);
	    } else {
		console.log('phantomjs child process exited succefully!');
	    }
	});
	//end of ScrapeMenu
    },
    
    'GetCategories'(dispensaryName){
	// Return category object {cagtegory: CategoryName, nItems: Nitems}
	var Menu =  Menus[dispensaryName];
	var menu = Menu.find().fetch();
	var categories = _.countBy(menu,function(item){return item.category;});
	categories = Object.keys(categories).map(function(key){
	    return {'category':key, 'nItems':categories[key]};
	});
	var categories_ordered = ['Indica', 'Sativa', 'Hybrid', 'Wax', 'Concentrate',
				  'Edible', 'Drink', 'Tincture', 'Preroll'];
	for(var i = 0; i < categories.length; i++) {
	    var found = 0;
	    for(var j = 0; j < categories_ordered.length; j++) {
		if(categories_ordered[j] == categories[i].category){
		    categories_ordered[j] = categories[i];
		    found = 1;
		    break;
		}
	    }
	    if (!found){
		categories_ordered.push(categories[i]);
	    }
	}
	
	return categories_ordered;
	//end of GetCategories
    }
    
// end of Methods  
});


